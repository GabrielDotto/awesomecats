export class LibGeral {
  public static estaPreenchido(valor: any): boolean {
    return !LibGeral.estaEmBranco(valor);
  }

  public static estaEmBranco(valor: any): boolean {
    return valor === undefined || valor === null || valor === '';
  }
}