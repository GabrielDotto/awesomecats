import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class CatsAPIService {

  constructor(private http: Http) { }


  getBreeds(search) {
    // index = 20 * (index + 1);
    
    const encodedURI  = encodeURI(`https://api.thecatapi.com/v1/breeds?name=${search}`)
    return this.http.get(encodedURI)
      .pipe(    
        map((response: Response) => response.json())
      );
  }

}

  
