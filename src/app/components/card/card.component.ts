import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CatInfosDialogComponent } from '../cat-infos-dialog/cat-infos-dialog.component';

@Component({
  selector: 'cat-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  entryComponents: [CatInfosDialogComponent]
  
})
export class CardComponent implements OnInit {

  @Input() cat: any;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDetalhesDialog(): void {
    console.log(this.cat);
    const dialogRef = this.dialog.open(CatInfosDialogComponent, {
      width: '50%',
      data: this.cat
    });
  }

}
