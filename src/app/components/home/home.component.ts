import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CatsAPIService } from 'src/app/services/cats-api.service';
import { MatButton, MatTabGroup, MatTabChangeEvent, MatSnackBar } from '@angular/material';
import { LibGeral } from 'src/app/helpers/libGeral';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public cats: any;

  @ViewChild('inputPesquisa', {static: false}) inputPesquisa: ElementRef;

  @ViewChild('btnVoltar', {static: false}) btnVoltar: MatButton;
  @ViewChild('btnAvancar', {static: false}) btnAvancar: MatButton;
  @ViewChild('tabGroup', {static: false}) tabGroup: MatTabGroup;
  public selectedTabIndex: number = 0;
  public quantidadeResultados: number = 0;

  constructor(private catService: CatsAPIService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    
  }

  navTabButtonClicked(value: number) {
    this.selectedTabIndex += value;
  }

  btnControl(event: MatTabChangeEvent) {
    this.btnAvancar.disabled = event.index === (this.tabGroup._tabs.length - 1) ? true : false;
    this.btnVoltar.disabled = event.index <= 0 ? true : false;
  }

  searchBreeds(search: string) {
    console.log("PESQUEISA", search);
    this.catService.getBreeds(search).subscribe(result => {

      this.quantidadeResultados = LibGeral.estaEmBranco(result) ? 0 : result.length;
      this.btnVoltar.disabled = true;
      this.btnAvancar.disabled = (LibGeral.estaPreenchido(result) && result.length <= 1) ? true : false;
      this.selectedTabIndex = 0;

      this.cats = result
      if(result.length == 0) {
        this.snackBar.open(`No breeds founded`, "OK");
      }
    },err => {
      this.snackBar.open(`Error to find breeds`, "OK");
    });
  }

  pesquisaCommand() {

  }

  mensagemBottomCard() {
    const mensagem = this.quantidadeResultados > 0
      ? `Breed ${this.selectedTabIndex + 1} of ${this.quantidadeResultados}`
      : `No breeds founded.`;
    return mensagem;
  }

}
