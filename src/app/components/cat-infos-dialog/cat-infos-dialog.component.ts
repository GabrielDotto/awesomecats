import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-cat-infos-dialog',
  templateUrl: './cat-infos-dialog.component.html',
  styleUrls: ['./cat-infos-dialog.component.scss']
})
export class CatInfosDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CatInfosDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
        console.log("DATA DO DIALOG", data);
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
