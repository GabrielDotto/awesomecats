import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatInfosDialogComponent } from './cat-infos-dialog.component';

describe('CatInfosDialogComponent', () => {
  let component: CatInfosDialogComponent;
  let fixture: ComponentFixture<CatInfosDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatInfosDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatInfosDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
